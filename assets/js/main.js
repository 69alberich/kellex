"use strict";


for (let el of document.querySelectorAll('.site-navigation li[class*="children"]')) {
    el.addEventListener('click', function (e) {
        e.target.classList.toggle('active');
        e.stopPropagation();
    });
}

for (let el of document.querySelectorAll('.menu-item-has-children > a')) {
    el.addEventListener("mouseover", function (e) {
        e.stopPropagation();
        var ul = e.target.nextElementSibling,
            isBig = ul.getBoundingClientRect().left + ul.offsetWidth > window.innerWidth;

        if (isBig) {
            ul.classList.add('to-left');
        }
    });
}

if (document.querySelector('.archive-posts')) {
    Macy({
        container: '.archive-posts',
        waitForImages: true,
        margin: {
            x: 50,
            y: 60
        },
        columns: 2,
        breakAt: {
            1024: 1
        }
    });
}

if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    // true for mobile device
    alert("mobile device");
  }else{
    // false for not mobile device
    alert("not mobile device");
  }