$(document).on('ajaxSetup', function(event, context) {
    // Enable AJAX handling of Flash messages on all AJAX requests
    context.options.flash = true

    // Enable the StripeLoadIndicator on all AJAX requests
    context.options.loading = $.oc.stripeLoadIndicator

    // Handle Error Messages by triggering a flashMsg of type error
    context.options.handleErrorMessage = function(message) {
        $.oc.flashMsg({ text: message, class: 'error' })
    }

    // Handle Flash Messages by triggering a flashMsg of the message type
    context.options.handleFlashMessage = function(message, type) {
        $.oc.flashMsg({ text: message, class: type })
    }
})

if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    // true for mobile device
    //alert("mobile device");
    $("#zones-banner, #plan-list-banner").addClass("d-block");
    $("#zones-video, #plan-list-video").addClass("d-none");
  }else{
    // false for not mobile device
  //  alert("not mobile device");
    $("#zones-banner, #plan-list-banner").addClass("d-none");
    $("#zones-video, #plan-list-video").addClass("d-block");
  }
  
$("document").ready(function(){

    $("#country-codes").selectpicker();

    $("#cityControl").on("change", function(){
        $(this).request('CityList::onSetCityIdPage', {
            
            'update': {
                'form/zone-list-options': '#zone-container'
            }
        }).then(function () {
            //alert("completado");
        });
    });

    $(".plan-link").on("click", function(e){
        e.preventDefault();
       var data = {
            'id': $(this).data("value")
        };
        
        $(this).request('ServiceForm::onSetPresentationIdPage', {
            data: data,
            'update': {
                'form/modal-plan-info': '#modal-plan-container'
            }
        }).then(function () {
            $('.plan-form-modal-lg').modal('show');
        });
       
    });

    $("#simetric-plan-link").on("click", function(e){
        e.preventDefault();
       var data = {
            'code': $(this).data("value")
        };
        
        
        
        $(this).request('ServiceForm::onSetServiceCodePage', {
            data: data,
            'update': {
                'form/modal-plan-info': '#modal-plan-container'
            }
        }).then(function () {
            $('.plan-form-modal-lg').modal('show');
        });
       
    });

    $("#subscribe-form").on("submit", function(e){
        e.preventDefault();
        $(this).request("formSubscribe::onAddSubscriber", {
            success: function(response){
                if (response.success) {
                    swal({
                        title: '¡Gracias por suscribirte!',
                        text: 'Ahora estarás al tanto de todos nuestros servicios',
                        button:true,
                    });
                }else{
                    swal({
                        icon: 'warning',
                        title: '¡UPS!',
                        text: 'Parece que ya te has suscrito a nuestro newsletter',
                        button:true,
                    })
                }
                

            },
            error: function(response){
               
                swal({
                    icon: 'warning',
                    title: '¡UPS!',
                    text: 'Parece que ha ocurrido un error, intenta nuevamente',
                    button:true,
                    });
            }
        });
    });

    $("#service-form").on("submit", function(e){
        e.preventDefault();

        var messages = "";
        const form = $(this).serializeArray();
        var obForm = convertToObject(form);
        
        messages = validateRegister(obForm);
        
        if(messages !=""){
            toastr.warning(messages, "Espera un segundo");
        }else{
            
            $(this).request("ServiceForm::onSendForm", {
            data:{
                phone: obForm.country_code+"-"+obForm.phone,
              },
              success: function(response){
                swal({
                title: '¡Gracias! hemos recibido tu solicitud',
                text: 'Nuestro equipo se pondrá en contacto contigo en las próximas 24 horas para concretar la contratación del servicio en caso de que podamos confirmar la factibilidad del mismo.',
                closeOnEsc:false,
                allowEnterKey: false,
                allowOutsideClick: false,
                button:true,
                }).then(() => {
                    console.log("aqui we");
                    location.reload();
                });
              },
              error: function(){

              }
            });
        }
        
    });


    function validateRegister(data){
        var errorMessage ="";
            
             if(data["name"]===""){
                errorMessage+="*Tu nombre es obligatorio <br>";
             }
             if(data["lastname"]===""){
                errorMessage+="*Tu apellido es obligatorio <br>";
             }
             if(data["email"]===""){
                errorMessage+="*Tu email es obligatorio <br>";
             }
            
            if(data["phone"]==="" && data["phone"].length < 7){
                errorMessage+="*Tu telefono es obligatorio y debe tener al menos 8 caracteres <br>";
            }

            if(data["zone_id"]==="" || data["zone_id"] < 1){
                errorMessage+="*Elige un minicípio y zona válida <br>";
            }

            if(data["address"]==="" || data["address"].length < 7){
                errorMessage+="*Tu dirección es obligatoria y debe tener al menos 8 caracteres <br>";
            }
            /*
            if(data["document"]==="" && data["document"].length < 7){
              errorMessage+="*Tu documento de identidad es obligatorio y debe tener al menos 7 caracteres <br>";
            }*/
      
             
        return errorMessage;
      }

      function convertToObject(array) {
        var obj = {};
        array.forEach(element => {
          obj[element.name] = element.value;
        });
        return obj;
      }
});
